#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import os

from PyQt5.QtWidgets import (
    QApplication, QGridLayout, QLabel,
    QPushButton, QWidget,
)

from PyQt5.QtGui import (
    QBrush, QColor, QFont, QIcon, QPainter,
    QPalette, QPixmap,
)

from PyQt5.QtCore import Qt

from src.Hammer import Palm, KidsHammer, AdultsHammer, Mjolnir, DummyHammer
from src.Button import Button

from config import (
    APP_DIR, APP_TITLE, APP_VERSION, WALL_IMAGE, BACKGROUND_IMAGE_PATH,
    DATA_PATH, DEFAULT_DATA, PALM_POWER, PALM_COST, KIDS_HAMMER_POWER,
    KIDS_HAMMER_COST, ADULTS_HAMMER_POWER, ADULTS_HAMMER_COST,
    MJOLNIR_POWER, MJOLNIR_COST
)

class App(QWidget):
    def __init__(self):
        super().__init__()
        self.money = 30
        self.hammers = {'Palm': Palm(), 'Kids hammer': KidsHammer(),
                        'Adults hammer': AdultsHammer(), 'Mjolnir': Mjolnir()}
        self.buttons = {'Button': Button()}
        self.current_hammer = DummyHammer()
        self.current_button = self.buttons['Button']

        self.hammers_buttons = [
            QPushButton(hammer.name) for hammer in self.hammers.values()
        ]
        self.button_button = QPushButton(self.buttons['Button'].name, self)

        self.data = {}
        self.layout = QGridLayout()
        self.money_label = QLabel(str(self.money))
        self.strength_label = QLabel(str(self.current_hammer.get_power()))
        self.money_changed = True

        self.initUI()

    def initUI(self):
        self.set_background()
        self.set_items()
        self.set_window_params()

    def paintEvent(self, event):
        if self.money_changed:
            qp = QPainter()
            qp.begin(self)
            qp.setPen(QColor(255, 255, 255))
            qp.setFont(QFont('Comic Sans MS', 36))
            qp.drawText(100, 40, self.money_label.text())
            qp.drawText(620, 40, self.strength_label.text())
            qp.end()
            self.money_changed = False

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent == Qt.Key_Space:
            self.apply_hammer()

    def apply_hammer(self):
        hit_result = self.current_hammer.hit(self.current_button)
        self.money += hit_result['hp']
        self.money_label.setText(str(self.money))
        self.strength_label.setText(str(self.current_hammer.get_power()))
        self.money_changed = True

        if hit_result['button_destroyed']:
            self.current_button = self.buttons['Button']

        for i, hammer_button in enumerate(self.hammers_buttons):
            if self.money >= self.hammers[hammer_button.text()].get_cost():
                self.hammers_buttons[i].setDisabled(False)

        self.update()

    def buy_hammer(self):
        self.button_button.setFocus(Qt.TabFocusReason)

        hammer = self.hammers[self.sender().text()]
        cost = hammer.get_cost()

        if cost <= self.money:
            self.money -= cost
            self.money_label.setText(str(self.money))
            self.current_hammer.set_power(self.current_hammer.get_power() +
                                          hammer.get_power() *
                                          bool(hammer.get_cost()))
            self.strength_label.setText(str(self.current_hammer.get_power()))
            self.money_changed = True

        for i, hammer_button in enumerate(self.hammers_buttons):
            if self.money < self.hammers[hammer_button.text()].get_cost():
                self.hammers_buttons[i].setDisabled(True)

        self.update()

    def set_background(self):
        palette = self.palette()
        bg_image = QPixmap(os.path.join(APP_DIR, BACKGROUND_IMAGE_PATH))
        bg_image = bg_image.scaled(960, 540)
        palette.setBrush(QPalette.Background, QBrush(bg_image))
        palette.setBrush(QPalette.ButtonText, QColor(Qt.transparent))
        self.setPalette(palette)

    def set_items(self):
        for pos in range(len(self.hammers_buttons) + 1):
            if pos >= len(self.hammers_buttons):
                hammer_button = QPushButton()
            else:
                hammer_button = self.hammers_buttons[pos]
            hammer_button.setDisabled(bool(pos))
            hammer_button.setFixedHeight(200)
            hammer_button.setFlat(True)
            hammer_button.clicked.connect(self.buy_hammer)
            self.layout.addWidget(hammer_button, *(0, pos))

        self.button_button.move(300, 420)
        self.button_button.setFixedSize(250, 100)
        self.button_button.setFlat(True)
        self.button_button.clicked.connect(self.apply_hammer)

    def set_window_params(self):
        self.setLayout(self.layout)
        self.setFixedSize(960, 540)
        self.setWindowTitle(APP_TITLE)
        self.setWindowIcon(QIcon(os.path.join(APP_DIR, BACKGROUND_IMAGE_PATH)))
        self.show()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    GUI = App()
    sys.exit(app.exec_())
