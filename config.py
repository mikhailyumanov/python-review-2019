import os

APP_DIR = os.path.dirname(os.path.realpath(__file__))
APP_TITLE = "SuperClicker2000"
APP_VERSION = "0.1.1"
WALL_IMAGE = "wall_debug.png"
BACKGROUND_IMAGE_PATH = os.path.join("img", WALL_IMAGE)
DATA_PATH = "data.json"
DEFAULT_DATA = "{1: 2}"  # TODO:

PALM_POWER = 5
PALM_COST = 0
KIDS_HAMMER_POWER = 23
KIDS_HAMMER_COST = 508
ADULTS_HAMMER_POWER = 305
ADULTS_HAMMER_COST = 8764
MJOLNIR_POWER = 9999999
MJOLNIR_COST = 9999999