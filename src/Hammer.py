from abc import ABC, abstractmethod
from config import *


class Hammer(ABC):
    """
    Abstract class for hammer.
    """
    _power = -1
    _cost = -1
    name = "Hammer"

    @abstractmethod
    def hit(self, button):
        raise NotImplementedError

    @abstractmethod
    def get_power(self):
        raise NotImplementedError

    @abstractmethod
    def set_power(self, new_power):
        raise NotImplementedError

    @abstractmethod
    def get_cost(self):
        raise NotImplementedError

    @property
    def __str__(self):
        return "<Hammer | {}>".format(self.name, self._power)


class Palm(Hammer):
    """`
    Class for Palm.
    """
    _power = PALM_POWER
    _cost = PALM_COST
    name = "Palm"

    def hit(self, button):
        result = button.take_hit(self)
        return result

    def get_power(self):
        return self._power

    def set_power(self, new_power):
        self._power = new_power

    def get_cost(self):
        return self._cost


class KidsHammer(Palm):
    """
    Class for Kids hammer.
    """
    _power = KIDS_HAMMER_POWER
    _cost = KIDS_HAMMER_COST
    name = "Kids hammer"


class AdultsHammer(Palm):
    """
    Class for Adults hammer.
    """
    _power = ADULTS_HAMMER_POWER
    _cost = ADULTS_HAMMER_COST
    name = "Adults hammer"


class Mjolnir(Palm):
    """
    Class for Mjolnir.
    """
    _power = MJOLNIR_POWER
    _cost = MJOLNIR_COST
    name = "Mjolnir"


class DummyHammer(Palm):
    """
    Class for Dummy hammer. It EXACTLY used while playing
    """
    name = "Dummy hammer"
