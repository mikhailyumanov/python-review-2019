class Button:
    """
    Class for button.
    """
    def __init__(self):
        self._health = 20
        self._bonus = None
        self.name = "Button"

    def take_hit(self, hammer):
        self._health = max(0, self._health - hammer.get_power())
        return {
            'hp': hammer.get_power(),
            'button_destroyed': self._health == 0,
            'bonus': self._bonus,
        }

    @property
    def get_health(self):
        return self._health

    @property
    def __str__(self):
        return '<Button | %s : %s>' % (self._health, self._bonus)


class RedButton(Button):
    def __init__(self):
        super().__init__()
        self._strength = 333
        self._bonus = "red_button"
        self.name = "Red button"


class FButton(Button):
    def __init__(self):
        super().__init__()
        self._strength = 8080
        self._bonus = "f_button"
        self.name = "F button"

